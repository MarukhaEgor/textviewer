package com.example.textviewer


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.text.method.ScrollingMovementMethod
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textViewer.movementMethod = ScrollingMovementMethod()
        applicationContext.assets.open("poem.txt").bufferedReader().use {
            textViewer.text = it.readText()
        }


        if (savedInstanceState != null) {
            if (savedInstanceState.get("Visibility") == VisibilityClass.VISIBLE.toString()) {
                settingsOfVisible()
            } else {
                settingsOfInvisible()
            }
        }

        switchButton.setOnClickListener {
            onClick()
        }
    }

    fun onClick() {
        if (textViewer.visibility == INVISIBLE) {
            settingsOfVisible()
        } else {
            settingsOfInvisible()
        }
    }

    /* setting of visible/invisible of elements on activity */
    fun settingsOfVisible(){
        switchButton.setBackgroundResource(R.drawable.button_background)
        switchButton.setText(getString(R.string.button_name_hide))
        textViewer.visibility = VISIBLE
    }

    fun settingsOfInvisible(){
        switchButton.setBackgroundResource(R.drawable.button_background_pressed)
        switchButton.setText(getString(R.string.button_name_show))
        textViewer.visibility = INVISIBLE
    }


    /**
     * save param of visibility of our text view
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            if (textViewer.visibility == VISIBLE) {
                putString("Visibility", "VISIBLE")
            } else {
                putString("Visibility", "INVISIBLE")
            }
        }
        super.onSaveInstanceState(outState)
    }


}


